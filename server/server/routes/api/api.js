const express = require('express');

const router = express.Router();

// Post
router.post('/', (req, res) => {
    const file = `${__dirname}` + req.body.path;
    res.download(file); // Set disposition and send it.
});

// Get
router.get('/', (req, res) => {
    res.send('hello');
});


module.exports = router;