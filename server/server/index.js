const express = require('express'),
      bodyParser = require('body-parser'),
      cors = require('cors'),
      mongoose = require('mongoose');

const app = express();

// Middleware
app.use(bodyParser.json());
app.use(cors());

const api = require('./routes/api/api');

app.use('/', api);

const port = process.env.PORT || 5000;

app.listen(port, () => console.log(`Server started on port ${port}`));
